module mod

go 1.22.2

require (
	github.com/parnurzeal/gorequest v0.3.0
	github.com/shirou/gopsutil v2.21.11+incompatible
	github.com/spf13/cast v1.6.0
	gitlab.com/kot0/tools v1.15.2
)

require (
	github.com/Pallinder/go-randomdata v1.2.0 // indirect
	github.com/elazarl/goproxy v0.0.0-20240726154733-8b0c20506380 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/smartystreets/goconvey v1.8.1 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	github.com/tklauser/go-sysconf v0.3.14 // indirect
	github.com/tklauser/numcpus v0.8.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	golang.org/x/net v0.27.0 // indirect
	golang.org/x/sys v0.23.0 // indirect
)
