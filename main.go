package main

import (
	"encoding/json"
	"fmt"
	"image"
	"image/png"
	"os"
	"os/exec"
	"runtime"
	"sync"
	"time"

	"github.com/parnurzeal/gorequest"
	"github.com/shirou/gopsutil/cpu"
	"github.com/spf13/cast"
	"gitlab.com/kot0/tools"
)

const SingleCoreIterations = 10
const MultiCoreIterations = 10

var dataSet []image.Image

var numCpus = runtime.NumCPU()

var allComputeVal int64 = 0

var c = make(chan image.Image, 100000)

var m sync.Mutex

func main() {
	fmt.Println("System info:", getSystemInfo())
	fmt.Println("")

	fmt.Println("Num cores:", numCpus)

	fmt.Println("Loading data set...")

	data, err := os.ReadDir("dataset")
	tools.OnErrorPanic(err)

	for _, file := range data {
		path := "dataset" + "/" + file.Name()
		// fmt.Println(path)

		file, err := os.Open(path)
		tools.OnErrorPanic(err)

		img, err := png.Decode(file)
		tools.OnErrorPanic(err)

		dataSet = append(dataSet, img)
	}

	for i := 0; i < numCpus; i++ {
		go computeWorker()
	}

	fmt.Println("Testing single core with " + tools.EncloseTextInBrackets(cast.ToString(SingleCoreIterations)) + " " + "iterations...")
	var singleCoreScore int
	for i := range SingleCoreIterations {
		tempScore := testSingleCore()
		fmt.Println("Single core result "+tools.EncloseTextInBrackets(cast.ToString(i+1)+"/"+cast.ToString(SingleCoreIterations))+":", tempScore, "best:", singleCoreScore)
		if tempScore < singleCoreScore || singleCoreScore == 0 {
			singleCoreScore = tempScore
		}
	}
	fmt.Println("Single core result:", singleCoreScore)

	fmt.Println("Testing multi core with " + tools.EncloseTextInBrackets(cast.ToString(MultiCoreIterations)) + " " + "iterations...")
	var multiCoreScore int
	for i := range MultiCoreIterations {
		tempScore := testMultiCore()
		fmt.Println("Multi core result "+tools.EncloseTextInBrackets(cast.ToString(i+1)+"/"+cast.ToString(MultiCoreIterations))+":", tempScore, "best:", multiCoreScore)
		if tempScore < multiCoreScore || multiCoreScore == 0 {
			multiCoreScore = tempScore
		}
	}
	fmt.Println("Multi core result:", multiCoreScore)

	fmt.Println("Uploading results to server...")

	jsonToSend, err := json.Marshal(&benchmarkResultStruct{
		SystemInfo:      getSystemInfo(),
		SingleCoreScore: singleCoreScore,
		MultiCoreScore:  multiCoreScore,
	})
	tools.OnErrorPanic(err)

	_, res, _ := gorequest.New().Post("http://pe7.org:3030/api").
		Send("benchResult" + "=" + tools.UrlEncode(string(jsonToSend))).
		End()

	// fmt.Println("res:", res)

	var benchmarkTopResults benchmarkTopApiReturnStruct

	err = json.Unmarshal([]byte(res), &benchmarkTopResults)
	tools.OnErrorPanic(err)

	fmt.Println("")

	fmt.Println("Top by single core:")
	for i, bResult := range benchmarkTopResults.TopBySingleCore {
		i += 1
		fmt.Println("#"+cast.ToString(i), "score:", bResult.SingleCoreScore, "system info:", bResult.SystemInfo)
	}

	fmt.Println("")

	fmt.Println("Top by multi core:")
	for i, bResult := range benchmarkTopResults.TopByMultiCore {
		i += 1
		fmt.Println("#"+cast.ToString(i), "score:", bResult.MultiCoreScore, "system info:", bResult.SystemInfo)
	}
}

func getSystemInfo() string {
	if runtime.GOOS == "windows" {
		v, _ := cpu.Info()
		return "cores: " + bitf(cast.ToString(runtime.NumCPU())) + " arch: " + bitf(runtime.GOARCH) + " name: " + bitf(v[0].ModelName)
	}

	// only amd64
	if runtime.GOOS == "linux" && runtime.GOARCH == "amd64" {
		v, _ := cpu.Info()
		return "cores: " + bitf(cast.ToString(runtime.NumCPU())) + " arch: " + bitf(runtime.GOARCH) + " name: " + bitf(v[0].ModelName)
	}

	// only linux arm
	if runtime.GOOS == "linux" && (runtime.GOARCH == "arm64" || runtime.GOARCH == "arm") {
		data, err := exec.Command("/bin/bash", "-c", "lscpu").CombinedOutput()
		tools.OnErrorPanic(err)

		lsCpu := string(data)

		modelName := tools.ParseValueStaticCompile(lsCpu, `Model name:\s*(?P<c>.*)`)
		cpuMaxMHz := tools.ParseValueStaticCompile(lsCpu, `CPU max MHz:\s*(?P<c>.*)`)

		tcpuInfo, err := os.ReadFile(`/proc/cpuinfo`)
		tools.OnErrorPanic(err)

		cpuInfo := string(tcpuInfo)

		boardName := tools.ParseValueStaticCompile(cpuInfo, `Hardware\s*:\s*(?P<c>.*)`)

		return "cores: " + bitf(cast.ToString(runtime.NumCPU())) + " arch: " + bitf(runtime.GOARCH) + " name: " + bitf(modelName) + " maxMHz: " + bitf(cpuMaxMHz) + " boardName: " + bitf(boardName)
	}

	fmt.Println("ERROR GOOS:", runtime.GOOS)
	return ""
}

func bitf(text string) string {
	return "(" + text + ")"
}

var trueComVal int64 = 2127794688862

func testSingleCore() int {
	runtime.GOMAXPROCS(1)

	var results []int

	for i := 0; i < 5; i++ {
		allComputeVal = 0

		t := time.Now()

		for _, img := range dataSet {
			c <- img
		}

		for allComputeVal != trueComVal {
			time.Sleep(1 * time.Microsecond)
			// time.Sleep(400 * time.Millisecond)
			// fmt.Println(allComputeVal)
		}

		t2 := time.Since(t)

		// fmt.Println(allComputeVal) // 2127794688862

		results = append(results, int(t2.Milliseconds()))
	}

	resultsSumm := 0
	for _, result := range results {
		resultsSumm += result
	}

	averageResult := resultsSumm / len(results)

	return averageResult
}

func testMultiCore() int {
	runtime.GOMAXPROCS(numCpus)

	var results []int

	for i := 0; i < 5; i++ {
		allComputeVal = 0

		t := time.Now()

		for _, img := range dataSet {
			c <- img
		}

		for allComputeVal != trueComVal {
			time.Sleep(1 * time.Microsecond)
			// time.Sleep(400 * time.Millisecond)
			// fmt.Println(allComputeVal)
		}

		t2 := time.Since(t)

		// fmt.Println(allComputeVal) // 2127794688862

		results = append(results, int(t2.Milliseconds()))
	}

	resultsSumm := 0
	for _, result := range results {
		resultsSumm += result
	}

	averageResult := resultsSumm / len(results)

	return averageResult
}

func computeWorker() {
	for {
		img := <-c

		imageComputeVal := int64(0)

		for x := 0; x < 300; x++ {
			for y := 0; y < 300; y++ {
				r, g, b, a := img.At(x, y).RGBA()
				preComputeVal := int64(r + g + b + a)
				imageComputeVal += preComputeVal
			}
		}

		m.Lock()
		allComputeVal += imageComputeVal
		m.Unlock()
	}
}

type benchmarkTopApiReturnStruct struct {
	TopBySingleCore []benchmarkResultStruct `json:"top_by_single_core"`
	TopByMultiCore  []benchmarkResultStruct `json:"top_by_multi_core"`
}

type benchmarkResultStruct struct {
	SystemInfo      string `json:"system_info"`
	SingleCoreScore int    `json:"single_core_score"`
	MultiCoreScore  int    `json:"multi_core_score"`
}
